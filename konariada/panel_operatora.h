#ifndef PANEL_OPERATORA_H
#define PANEL_OPERATORA_H

#include <QMainWindow>
#include <display.h>
#include <pytanie.h>
#include <QCheckBox>
#include <vector>
#include <QSound>
#include <QSignalMapper>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QString>
#include <QStringList>
#include <QGridLayout>



namespace Ui {
class panel_operatora;
}

class panel_operatora : public QMainWindow
{
    Q_OBJECT

public:
    explicit panel_operatora(QWidget *parent = 0);
    ~panel_operatora();
    void clear_answear_checkbox();
    bool parse_file(QString name);
    void zeruj_pule(quint8 iloscOdp=5);

private slots:
    void on_checkBox_clicked();
    //void disconnect_dynamic();

    void on_comboBox_currentIndexChanged(int index);
    void dodajDoPuli(QString tmp);
    QString openAnswearFile();
    void nowa_baza();

    void on_gibPointsToA_clicked();

    void on_gibPointsToB_clicked();

    void on_lineEdit_returnPressed();

    void on_failA_clicked();

    void on_failB_clicked();
    void pokazOdp(QString odp);

    void on_clearA_clicked();

    void on_clearB_clicked();

    void on_animate_clicked();

private:
    Ui::panel_operatora *ui;
    display *okno;
    std::vector<pytanie> baza_pytan;
    std::vector <QCheckBox*> czekboksy;
    QSound* darude;
    QSound* failSound;
    QSound* correctSound;
    QSound* rundaSound;
    QSound* startSound;
    QSound* gameOverSound;
    qint16 pula;
    QSignalMapper* signalMapper;
    QSignalMapper* signalMapper2;
    QGridLayout *mainLayout;


};

#endif // PANEL_OPERATORA_H
