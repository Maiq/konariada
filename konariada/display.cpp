#include "display.h"
#include "ui_display.h"
#include "QFont"
#include "QDebug"
#include "QTimer"
#include "QTransform"
#include <cmath>

#define MAXLEN 35 // maximum liter

display::display(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::display)
{
    ui->setupUi(this);
    ui->nameTeamA->setText("Team A");
    ui->nameTeamB->setText("Team B");
    ui->failA1->setText("");
    ui->failA2->setText("");
    ui->failA3->setText("");
    ui->failB1->setText("");
    ui->failB2->setText("");
    ui->failB3->setText("");

    answears.push_back(ui->answear1);
    answears.push_back(ui->answear2);
    answears.push_back(ui->answear3);
    answears.push_back(ui->answear4);
    answears.push_back(ui->answear5);
    answears.push_back(ui->answear6);
    answears.push_back(ui->answear7);
    answears.push_back(ui->answear8);
    answears.push_back(ui->answear9);
    answears.push_back(ui->answear10);
    answears.push_back(ui->answear11);
    answears.push_back(ui->answear12);

    QFont a;
    a.setPointSize(18);

    foreach(auto it, answears){
        it->setFont(a);
        it->setText(".....................................");
    }
    pxmp=NULL;
//    pxmp.load(":/new/prefix1/bb8_2.png");
//    pxmp.scaled(20,40,Qt::KeepAspectRatio);
    qx=10;
    qy=10;

    timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(update()));
        timer->start(15);

}

void display::setFail(qint8 team){

    if(team=='A')
    {
        if(ui->failA1->text()=="")
        {
            ui->failA1->setText("X");
            return;
        }
        else if(ui->failA2->text()=="")
        {
            ui->failA2->setText("X");
            return;
        }
        else
        {
            ui->failA3->setText("X");
        }
    }
    else
    {
        if(ui->failB1->text()=="")
        {
            ui->failB1->setText("X");
            return;
        }
        else if(ui->failB2->text()=="")
        {
            ui->failB2->setText("X");
            return;
        }
        else
        {
            ui->failB3->setText("X");
        }
    }
    update();
}

void display::paintEvent(QPaintEvent *){
    QPainter pntr(this);

    pntr.translate(0,0);
    pntr.rotate(0);
    static float fx=0;
    static float fy=0;
    if(pxmp!=NULL)
    {
//        t.translate(1,1);
//        pntr.setTransform(t);

        pntr.drawPixmap(fx,(height()/2)+abs(150*cosf(0.005*fx)),*pxmp);
        fx+=7;
    }
    if(fx>width())
    {
        fx=0;
        delete pxmp;
        pxmp=NULL;
    }


}

void display::animate(qint8 howMany)
{

    if(howMany==0)
    {
        delete pxmp;
        pxmp=NULL;
        update();
        return;
    }
    if(pxmp==NULL){
    pxmp = new QPixmap();
    pxmp->load(":/new/prefix1/bb8_2.png");
    }
    //update();

}

void display::clearFail(qint8 team)
{
    if(team=='A')
    {
        ui->failA1->setText("");
        ui->failA2->setText("");
        ui->failA3->setText("");
    }
    if(team=='B')
    {
        ui->failB1->setText("");
        ui->failB2->setText("");
        ui->failB3->setText("");
    }
}

void display::clearFails()
{
    ui->failA1->setText("");
    ui->failA2->setText("");
    ui->failA3->setText("");
    ui->failB1->setText("");
    ui->failB2->setText("");
    ui->failB3->setText("");
}

void display::setName(qint8 team, QString s)
{
    if(team=='A')
    {
        ui->nameTeamA->setText(s);
    }
    else
    {
        ui->nameTeamB->setText(s);
    }

}

void display::addPoints(qint8 team, qint16 p)
{
    if(team=='A')
    {
        ui->lcdTeamA->display(p);
    }
    else
    {
        ui->lcdTeamB->display(p);
    }
}

void display::showAnswear(qint8 position, QString str)
{
//    QString txt;
//    quint16 count;
//    qDebug()<<str;
//    if(str=="")
//    {
//       answears[position]->setText(str);
//    }
//    if(position < answears.size())
//    {
//       txt=QString::number(position+1);
//       if(str.size()<MAXLEN)
//       {
//           count=str.length();
//           count=MAXLEN-str.length();
//           txt=txt + ". " +str;
//       }
//    }
    answears[position]->setText(str);
}

void display::addToPool(qint16 p)
{
    ui->lcdPool->display(p);
}

display::~display()
{
    delete ui;
}
