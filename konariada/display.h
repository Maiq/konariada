#ifndef DISPLAY_H
#define DISPLAY_H

#include <QDialog>
#include <QLabel>
#include <QPainter>
#include <QPaintEvent>
#include <QPixmap>
#include <QTimer>
#include <QTransform>
namespace Ui {

class display;
}

class display : public QDialog
{
    Q_OBJECT

public:
    explicit display(QWidget *parent = 0);
    void setFail(qint8 team);
    void clearFail(qint8 team);
    void addPoints(qint8 team, qint16 p);
    void setName(qint8 team, QString s);
    void clearFails();
    void addToPool(qint16 p);
    ~display();

    void showAnswear(qint8 position, QString str);

    void paintEvent(QPaintEvent *);
    void animate(qint8 howMany);
    QTransform t;

private:
    Ui::display *ui;
    std::vector<QLabel*> answears;
    QPainter *drawing=NULL;
    QPixmap *pxmp;
    QPixmap *pxmp2;
    QTimer *timer;
    int qx;
    int qy;
};

#endif // DISPLAY_H
