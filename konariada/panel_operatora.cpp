#include "panel_operatora.h"
#include "ui_panel_operatora.h"
#include <QPushButton>
#include <QDebug>
#include <QFileDialog>
#include <QFileInfo>
#include <utility>
#include <algorithm>
#define CB_Y 160
panel_operatora::panel_operatora(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::panel_operatora)
{

    ui->setupUi(this);
    okno = new display();
    okno->show();
    pula=0;
    signalMapper=nullptr;
//    connect(signalMapper,SIGNAL(mapped(int)),this,SLOT(dodajDoPuli(int)));
//    ui->retranslateUi(this);
//    darude = new QSound(":/new/prefix1/Sandstorm.wav");
//   // connect(czekboksy[0],SIGNAL(clicked()),darude,SLOT(play()));

//    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(disconnect_dynamic()));
    connect(ui->action_aduj_Baz,SIGNAL(triggered()),this,SLOT(openAnswearFile()));
    //darude = new QSound(":/new/prefix1/Sandstorm.wav");
    failSound=new QSound(":/new/prefix1/failwav.wav");
    correctSound=new QSound(":/new/prefix1/goodwav.wav");
    rundaSound=new QSound(":/new/prefix1/rundawav.wav");
    startSound=new QSound(":/new/prefix1/startwav.wav");
    gameOverSound=new QSound("C:/Users/Maiqel/Documents/konariada/konariada/gameoverwav.wav");


    //connect(ui->checkBox,SIGNAL(clicked()),darude,SLOT(play()));
    connect(ui->failA,SIGNAL(clicked()),failSound,SLOT(play()));
    connect(ui->failB,SIGNAL(clicked()),failSound,SLOT(play()));

    QGridLayout *tl=new QGridLayout();
    ui->centralWidget->setLayout(tl);
    ui->verticalLayout->setSizeConstraint(QLayout::SetMinimumSize);
    ui->adminCombo->addItem("Name Team A");
    ui->adminCombo->addItem("Name Team B");
    ui->adminCombo->addItem("Play start");
    ui->adminCombo->addItem("Play game over");
    ui->adminCombo->addItem("Change Points A");
    ui->adminCombo->addItem("Change Points B");
}

panel_operatora::~panel_operatora()
{
    delete ui;
}

void panel_operatora::on_checkBox_clicked()
{
    if(okno->isFullScreen())
    {
        okno->showNormal();
    }
    else
    {
        okno->showFullScreen();
    }
}

bool panel_operatora::parse_file(QString name)
{
    pytanie tmp_p;
    QString tmp_s;
    QString line;
    QStringList lista;
    qint8 cntr=0;
    QFile file(name);
    if (!file.open(QIODevice::ReadOnly)) {
            qDebug() << file.errorString();
            return false;
        }
       while (!file.atEnd()) {
            line = file.readLine();
            lista=line.split(",", QString::SkipEmptyParts);
            cntr=0;
           foreach(auto a, lista)
           {
               if(a=="\n"){continue;}
               if(cntr==0)
               {
                tmp_p.tresc=a;
                }
               else
               {
                   if(cntr%2==1){
                       tmp_s=a;
                   }
                   else{
                       tmp_p.odpowiedz.insert(std::pair<QString,qint8>(tmp_s,a.toInt()));
                   }
               }
               cntr++;
           }
           baza_pytan.push_back(tmp_p);
           tmp_p.odpowiedz.clear();
       }
       file.close();
       return true;
}


void panel_operatora::nowa_baza()
{
    QCheckBox *tmp;

    foreach(auto a, baza_pytan)
    {
        ui->comboBox->addItem(a.tresc);
    }
}



QString panel_operatora::openAnswearFile()
{
    QString fileName = QFileDialog::getOpenFileName(this,
         tr("Open CSV"), "/home/", tr("CSV File (*.csv)"));
    qDebug()<< fileName;

    parse_file(fileName);
    QFileInfo check(fileName);
    if(check.exists() && check.isFile())
    {
    nowa_baza();
    }
    return fileName;
}

void panel_operatora::dodajDoPuli(QString tmp)
{
    auto id = ui->comboBox->currentIndex();
    pytanie tmpPyt= baza_pytan[id];
    std::vector<std::pair<QString,qint16>>pary;

    auto tmpP=tmpPyt.odpowiedz.find(tmp);
    qint16 p=0;
    std::pair<QString,qint16> tmpPair;

    foreach(auto tmpP, tmpPyt.odpowiedz)
    {
       tmpPair=std::make_pair(tmpP.first,tmpP.second);
       pary.push_back(tmpPair);
    }

    std::sort(pary.begin(),pary.end(),[](std::pair<QString,qint16> A,std::pair<QString,qint16> B){return A.second > B.second;});


    foreach(auto T, pary)
    {
        if(T.first==tmp)
        {
            okno->showAnswear(p,T.first + "  " + QString::number(T.second));
            pula+=T.second;
            break;
        }
        p++;
    }




    ui->pulaPunktow->display(pula);
    okno->addToPool(pula);

}

void panel_operatora::clear_answear_checkbox()
{
    foreach(auto a, czekboksy)
    {
        delete a;
    }
    czekboksy.clear();

}

void panel_operatora::zeruj_pule(quint8 iloscOdp)
{
    pula=0;
    ui->pulaPunktow->display(pula);
    okno->addToPool(pula);
    for(int i=0;i<12;i++)
    {
        if(i<iloscOdp){
        okno->showAnswear(i,".....................................");
        }
        else
        {
            okno->showAnswear(i,"");
        }
    }
}

void panel_operatora::pokazOdp(QString odp)
{
    okno->showAnswear(1,odp);
}

void panel_operatora::on_comboBox_currentIndexChanged(int index)
{
      rundaSound->play();
      clear_answear_checkbox();
      zeruj_pule(baza_pytan[ui->comboBox->currentIndex()].odpowiedz.size());
      okno->clearFails();
      QCheckBox *tmp;

      if(signalMapper!=nullptr){
          delete signalMapper;
      }
      signalMapper=new QSignalMapper(this);


      foreach(auto it, baza_pytan[ui->comboBox->currentIndex()].odpowiedz)
      {
          tmp=new QCheckBox();

          tmp->setText((it.first + " : " +QString::number(it.second)));
          tmp->setMinimumHeight(20);
          ui->verticalLayout->addWidget(tmp);
          connect(tmp,SIGNAL(clicked()),signalMapper,SLOT(map()));
        //  connect(tmp,SIGNAL(clocked()),signalMapper2,SLOT(map()));
          connect(tmp,SIGNAL(clicked(bool)),tmp,SLOT(setDisabled(bool)));
          connect(tmp,SIGNAL(clicked()),correctSound,SLOT(play()));
          czekboksy.push_back(tmp);
          signalMapper->setMapping(tmp,it.first);
          //signalMapper2->setMapping(tmp,it.first);
      }
      connect(signalMapper,SIGNAL(mapped(QString)),this,SLOT(dodajDoPuli(QString)));
      //connect(signalMapper2,SIGNAL(mapped(QString)),this,SLOT(pokazOdp(QString)));
    qDebug()<<" KONIEC ";
    qDebug()<<baza_pytan[ui->comboBox->currentIndex()].tresc;
    okno->animate(1);
}


void panel_operatora::on_gibPointsToA_clicked()
{
     qint16 tmp=static_cast<qint16>(ui->LcdTeamA->value());
     tmp+=static_cast<qint16>(ui->multiplyPool->value()*ui->pulaPunktow->value());
     ui->LcdTeamA->display(tmp);
//     zeruj_pule();
     okno->addPoints('A',tmp);
}


void panel_operatora::on_gibPointsToB_clicked()
{
    qint16 tmp=static_cast<qint16>(ui->LcdTeamB->value());
    tmp+=static_cast<qint16>(ui->multiplyPool->value()*ui->pulaPunktow->value());
    ui->LcdTeamB->display(tmp);
//    zeruj_pule();
    okno->addPoints('B',tmp);
}

void panel_operatora::on_lineEdit_returnPressed()
{
    QString t = ui->lineEdit->text();
    QString sw= ui->adminCombo->currentText();
    qDebug() << "Switch : " << sw;
    if(sw == "Name Team A"){
            ui->nameTeamA->setText(t);
            okno->setName('A',t);
        }
    else if(sw == "Name Team B"){
            ui->nameTeamB->setText(t);
            okno->setName('B',t);
        }
    else if(sw == "Play start")
    {
            startSound->play();
    }
    else if(sw == "Play game over"){
             gameOverSound->play();
    }
    else if(sw == "Change Points A")
    {
            ui->LcdTeamA->display(t.toInt());
            okno->addPoints('A',t.toInt());
    }
    else if(sw == "Change Points B")
    {
            ui->LcdTeamB->display(t.toInt());
            okno->addPoints('B',t.toInt());
    }
    else{
            ui->lineEdit->setText("");
        }

}

void panel_operatora::on_failA_clicked()
{
    okno->setFail('A');

}

void panel_operatora::on_failB_clicked()
{
    okno->setFail('B');

}

void panel_operatora::on_clearA_clicked()
{
    okno->clearFail('A');
}



void panel_operatora::on_clearB_clicked()
{
    okno->clearFail('B');
}

void panel_operatora::on_animate_clicked()
{
    static int lol=0;
    lol%=2;
    okno->animate(lol);
    lol++;
}
