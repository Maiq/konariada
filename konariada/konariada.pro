#-------------------------------------------------
#
# Project created by QtCreator 2016-04-15T22:53:30
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = konariada
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        panel_operatora.cpp \
    display.cpp \
    pytanie.cpp

HEADERS  += panel_operatora.h \
    display.h \
    pytanie.h

FORMS    += panel_operatora.ui \
    display.ui

RESOURCES += \
    sounds.qrc

DISTFILES +=
